#!/bin/sh

gcc -c -o time.o time.c;
ar rcs libtime.a time.o;

rustc main.rs -Lnative="${PWD}" -lstatic=time;