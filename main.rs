#![allow(non_snake_case)]
#![allow(unused_parens)]

const SYMBOLS: [[i32; 15]; 12] = [
	//0
	[
		1, 1, 1,
		1, 0, 1,
		1, 0, 1,
		1, 0, 1,
		1, 1, 1
	],
	
	//1
	[
		0, 1, 0,
		1, 1, 0,
		0, 1, 0,
		0, 1, 0,
		1, 1, 1
	],
	
	//2
	[
		1, 1, 1,
		0, 0, 1,
		1, 1, 1,
		1, 0, 0,
		1, 1, 1
	],
	
	//3
	[
		1, 1, 1,
		0, 0, 1,
		1, 1, 1,
		0, 0, 1,
		1, 1, 1
	],
	
	//4
	[
		1, 0, 1,
		1, 0, 1,
		1, 1, 1,
		0, 0, 1,
		0, 0, 1
	],
	
	//5
	[
		1, 1, 1,
		1, 0, 0,
		1, 1, 1,
		0, 0, 1,
		1, 1, 1
	],
	
	//6
	[
		1, 1, 1,
		1, 0, 0,
		1, 1, 1,
		1, 0, 1,
		1, 1, 1
	],
	
	//7
	[
		1, 1, 1,
		0, 0, 1,
		0, 0, 1,
		0, 0, 1,
		0, 0, 1
	],
	
	//8
	[
		1, 1, 1,
		1, 0, 1,
		1, 1, 1,
		1, 0, 1,
		1, 1, 1
	],
	
	//9
	[
		1, 1, 1,
		1, 0, 1,
		1, 1, 1,
		0, 0, 1,
		1, 1, 1
	],
	
	//: 
	[
		0, 0, 0,
		0, 1, 0,
		0, 0, 0,
		0, 1, 0,
		0, 0, 0
	],
	
	//Space
	[
		0, 2, 2,
		0, 2, 2,
		0, 2, 2,
		0, 2, 2,
		0, 2, 2
	]
];
const PIXEL_ON: &str  = "\u{2588}\u{2588}";
const PIXEL_OFF: &str = "  ";
const ONE_SECOND: std::time::Duration = std::time::Duration::from_secs(1);

fn main(){

    loop{
		unsafe{
			clearScreen();
		}
		
		print!("\n\n");
		printCurrentTime();
		
		
		std::thread::sleep(ONE_SECOND);
	}
}

fn printCurrentTime(){
	unsafe{
		printSymbols(&getCurrentTime());
	}
}

unsafe fn getCurrentTime() -> String{
	return format!("{:02}:{:02}:{:02}", getCurrentHours(), getCurrentMinutes(), getCurrentSeconds());
}

#[link(name = "time")]
extern "C"{
	fn getCurrentHours() -> i32;
	fn getCurrentMinutes() -> i32;
	fn getCurrentSeconds() -> i32;
	fn clearScreen();
}

fn printSymbols(s: &str){
	for r in 0..5{
		for c in s.chars(){
			printSymbolRow(c, r);
			printSymbolRow(' ', r);
		}
		println!();
	}
}

fn printSymbolRow(c: char, r: usize){
	let index =
    	if(c == ':'){
    	    10
    	}
    	else if(c == ' '){
    	    11
    	}
    	else{
    	    c as u8 - b'0'
    	};
	
	for i in (r * 3)..((r + 1) * 3){
	    if(SYMBOLS[index as usize][i] == 0){
	        print!("{}", PIXEL_OFF);
	    }
	    else if(SYMBOLS[index as usize][i] == 1){
	        print!("{}", PIXEL_ON);
	    }
	}
}