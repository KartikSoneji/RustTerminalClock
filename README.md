# Terminal Clock in Rust 
Simple terminal clock written in pure Rust, with a C Foreign Function Interface.

## To build
- Clone repository.
- Run: `build.sh`. (Will work on Windows as well)
- Run: `main`.

## Screenshot
![Terminal Clock](screenshot.gif)
