#include <time.h>
#include <stdio.h>
#include <stdlib.h>

struct tm* getCurrentTime(){
	time_t t = time(NULL);
	return localtime(&t);
}

int getCurrentHours(){
	return getCurrentTime()->tm_hour;
}

int getCurrentMinutes(){
	return getCurrentTime()->tm_min;
}

int getCurrentSeconds(){
	return getCurrentTime()->tm_sec;
}

void clearScreen(){
	#ifdef _WIN32
		system("cls");
	#else
		system("clear");
	#endif
}